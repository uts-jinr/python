# Проверьте, принадлежит ли точка (x,y) кругу с центром (xc, yc) и радиусом r.

# Решение должно содержать функцию IsPointInCircle(x, y, xc, yc, r),
# возвращающую True, если точка принадлежит кругу и False, если не
# принадлежит.
#
# Основная программа должна считать координаты точки, вызвать функцию
# IsPointInCircle и в зависимости от возвращенного значения вывести на экран
# необходимое сообщение. Функция IsPointInCircle не должна содержать
# инструкцию if.


def IsPointInCircle(x, y, xc, yc, r):
    #    xc_positiv = VarPositive(xc)
    #    yc_positiv = VarPositive(yc)
    #    x_positiv = VarPositive(x)
    #    y_positiv = VarPositive(y)

    x_dist = x - xc
    y_dist = y - yc

    slice = ((x - xc) * (x - xc) + (y - yc) * (y - yc)) ** 0.5
    #    print("slice =", slice)
    dist = slice - r
    #    print("dist = ", dist)
    # if dist <=0 so the point is INSIDE circle
    return IsShorter(dist)


def IsShorter(a):
    return a <= 0


def VarPositive(a):
    if a < 0:
        a *= -1
    return a


x = float(input())
y = float(input())
xc = float(input())
yc = float(input())
r = float(input())

if IsPointInCircle(x, y, xc, yc, r):
    print("YES")
else:
    print("NO")
