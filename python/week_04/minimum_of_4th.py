# Напишите функцию min4(a, b, c, d), вычисляющую минимум четырех чисел,
# которая не содержит инструкции if, а использует стандартную функцию min
# от двух чисел. Считайте четыре целых числа и выведите их минимум.


def min4(a, b, c, d):
    min_ab = min(a, b)
    min_abc = min(min_ab, c)
    return min(min_abc, d)


a = int(input())
b = int(input())
c = int(input())
d = int(input())

min_abcd = min4(a, b, c, d)

print(min_abcd)
