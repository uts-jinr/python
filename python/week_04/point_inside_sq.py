# Решение должно содержать функцию IsPointInSquare(x, y), возвращающую True,
# если точка принадлежит квадрату и False, если не принадлежит. Основная
# программа должна считать координаты точки, вызвать функцию IsPointInSquare и
# в зависимости от возвращенного значения вывести на экран необходимое
# сообщение. Функция IsPointInSquare не должна содержать инструкцию if.


def IsPointInSquare(x, y):
    x_positiv = VarPositive(x)
    y_positiv = VarPositive(y)
    return pos_inside(x_positiv) and pos_inside(y_positiv)


def VarPositive(a):
    if a < 0:
        a *= -1
    return a


def pos_inside(a):
    return a <= 1.0


x = float(input())
y = float(input())

if IsPointInSquare(x, y):
    print("YES")
else:
    print("NO")
