# Дано натуральное число n>1. Проверьте, является ли оно простым. Программа
# должна вывести слово YES, если число простое и NO, если число составное.
# Решение оформите в виде функции IsPrime(n), которая возвращает True для
# простых чисел и False для составных чисел.


def IsPrime(n):
    limit = int(n ** 0.5)
    i = 2
    while i <= limit:
        #  n % i != n or
        if n % i == 0:
            #            print("Делитель :", i)
            #            print(i)
            return "NO"
        else:
            i += 1
    #    print("Число", n, "простое, наименьший делитель", n)
    #     print(n)
    return "YES"


n = int(input())
print(IsPrime(n))
