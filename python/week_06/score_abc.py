# Сохраните в массивах список всех участников и выведите его, отсортировав по
# фамилии в лексикографическом порядке. При выводе указываете фамилию, имя
# участника и его балл.
# 30.06.2019 Сдано с 1 раза !


class Score:
    last_n = ''
    name = ''
    scl = 0
    point = 0


data = []
mysum = 0
score = Score()
inFile = open('input.txt', 'r', encoding='utf8')
for line in inFile:
    myline = line.strip('\n')
    # print(myline)
    # data = myline.split(sep=' ')
    last_n, name, scl, point = myline.split(sep=' ')
    score.last_n = last_n
    score.name = name
    score.scl = int(scl)
    score.point = int(point)
    data.append((last_n, name, score.scl, score.point))
    # print(myline)
inFile.close()
data.sort()
for names in data:
    print(names[0], names[1], names[3])
