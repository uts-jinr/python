# Для поступления в вуз абитуриент должен предъявить результаты трех
# экзаменов в виде ЕГЭ, каждый из них оценивается целым числом от 0 до 100
# баллов. При этом абитуриенты, набравшие менее 40 баллов
# (неудовлетворительную оценку) по любому экзамену из конкурса выбывают.
# Остальные абитуриенты участвуют в конкурсе по сумме баллов за три экзамена.
#
# В конкурсе участвует N человек, при этом количество мест равно K. Определите
# проходной балл, то есть такое количество баллов, что количество участников,
# набравших столько или больше баллов не превосходит K, а при добавлении к ним
# абитуриентов, набравших наибольшее количество баллов среди непринятых
# абитуриентов, общее число принятых абитуриентов станет больше K.
#
# Программа получает на вход количество мест K. Далее идут строки с
# информацией об абитуриентах, каждая из которых состоит из имени (текстовая
# строка содержащая произвольное число пробелов) и трех чисел от 0 до 100,
# разделенных пробелами.
#
# Используйте для ввода файл input.txt с указанием кодировки utf8 (для
# создания такого файла на своем компьютере в программе Notepad++ следует
# использовать кодировку UTF-8 без BOM).


# Открываем файл с исходными данными
inFile = open('input.txt', 'r', encoding='utf8')
data = []
ab_sum = 0
# Считываем 1 строку, количество мест из файла
K = int(inFile.readline().strip())
# Далее перебираем абитуриентов
for line in inFile:
    myline = line.strip()
    line = myline.split(sep=' ')
    # получаем баллы абитуриента, - 3 последние эл-та списка
    name = line[:-3]
    point_1, point_2, point_3 = line[-3:]
    if int(point_1) >= 40 and int(point_2) >= 40 and int(point_3) >= 40:
        ab_sum = int(point_1) + int(point_2) + int(point_3)
        data.append((name, int(point_1), int(point_2), int(point_3), ab_sum))
    # data.append(line)
inFile.close()
# print(data[2])
# for stud in data:
#    print(stud)
# А вот дальше начинаются варианты :
# 1. Создать список кортежей [индекс абика][баллы абика]
#   отсортировать по убыванию баллов (последний эл-т кортажа) и набрать
#   первые K элементов
# 2. Сразу отсортировать data[], используюя лямбда ф-цию
#
data.sort(key=lambda x: x[4], reverse=True)
# for stud in data:
#    print(stud)
# Теперь определяем проходной балл
# ДЛя этого нужна сортировка подсчетом, чтобы знать, сколько
# абиков набрало какой балл
# Список возможных баллов - от 0 до 300
scores = [0] * 301
for element in data:
    scores[element[4]] += 1
# print(*scores)
# !!!!!!!!!!!!!!!!!!!!!! Работает !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
result = []
ab_total = 0
for index in range(len(scores)):
    if scores[index] > 0:
        result.append((index, scores[index]))
        ab_total += scores[index]
        # print(index, scores[index])
    # for i in range(scores[index]):
    # print(index, scores[index])
# print(*result)
result.sort(reverse=True)
# print(*result)
# print("total number of pretenders", ab_total)
threshold = 0
the_point = 0
num = 0
# ab_total = 0
for i in range(len(result)):
    # print(result[i][1])
    num = result[i][1] + threshold
    if num <= K:
        threshold += result[i][1]

    else:
        the_point = result[i - 1][0]
        num -= result[i][1]
        break

if result[0][1] > K:
    the_point = 1
# print("Number of passed students", num)
# print("Minimal score is", the_point)
outFile = open('output.txt', 'w', encoding='utf8')
print(the_point, file=outFile)
outFile.close()
# ab_sum = 0
# for i in range(1,4):
#    ab_sum += data[2][i]
# print(ab_sum)
