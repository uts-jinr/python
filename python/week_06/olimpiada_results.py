# Упорядочите список участников олимпиады в порядке убывания набранных баллов.
# Программа получает на вход число участников олимпиады N. Далее идет N строк,
# в каждой строке записана фамилия участника, затем, через пробел, набранное
# им количество баллов.


N = int(input())
data = []
for i in range(N):
    last_n, point = list(input().split())
    # print(last_n, point)
    data.append((int(point), last_n))
    # print(data[i])
    # list(map(int, input().split()))
    # last_n, point = myline.split(sep=' ')
# for i in range(N):
#  print(*data[i][1])
data.sort(reverse=True)
for i in range(N):
    print(data[i][1])
