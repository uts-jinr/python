# В первой строке вводится число n - количество селений (1 <= n <= 100000).
# Вторая строка содержит n различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го селения.
# В третьей строке входных данных задается число m - количество бомбоубежищ
# (1 <= m <= 100000).
# Четвертая строка содержит m различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го бомбоубежища. Все  расстояния
# положительны и не превышают 10⁹.
# Селение и убежище могут располагаться в одной точке.
#
# Чтобы спасение в случае ядерной тревоги проходило как можно эффективнее,
# необходимо для каждого селения определить ближайшее к нему бомбоубежище.


from math import fabs


# принимает кортеж, индекс элемента в кортеже, возвращает элемент кортежа по
# индексу
def sortFunc(obj, index):
    return obj[index]


# Вводим число селений
n = int(input())
# Формируем список из значений расстояния от НАЧАЛА дороги до селения
# len(n_length) == n
n_length = list(map(int, input().split()))
# Вводим количество убежищ
m = int(input())
# Формируем список значений расстояния от НАЧАЛА дороги до убежища
# len(m_length) == m
m_length = list(map(int, input().split()))

# СОздать список кортежей, Где кортеж - это пара [индекс селения, расстояние]
# Отсортировать список по расстоянию...
# print(*n_length)

n_list = []
i = 0
for i in range(n):
    n_list.append((i + 1, n_length[i]))
#
# Сортируем список из кортежей по 2-му эл-ту кортежа, т.е. по расстоянию
# Сохраняем в отдельный список
# CCуки, почему не сказали про ф-цию lambda ???
n_list_sorted = sorted(n_list, key=lambda x: x[1])
# print(*n_list_sorted)
# print(sorted(n_list, key=lambda x: x[1]))
#
# Создаем список из кортежей [номер убежища, расстояние для убежища]
m_list = []
i = 0
for i in range(m):
    m_list.append((i + 1, m_length[i]))
    
# Сортируем список по 2 эл-ту кортежа
m_list_sorted = sorted(m_list, key=lambda x: x[1])
# print(*m_list_sorted)

# Так тоже работает сортировка по 2-му эл-ту кортежа в списке кортежей
# n_list.sort(key=sortFunc)
# print(*n_list)
# Теперь проходим по обоим сортированным спискам и формируем ответ
# на задачу, т.е. итоговый список.
#
# Получаем уже решенную ранее задачу:
# В заданной последовательности чисел найти число, ближайшее к заданному


def get_closest_value(num, a):
    #
    # a = list(map(int, input().split()))
    # num = int(input())
    diff = 1000000000
    for i in a:
        diff_act = int(fabs(i - num))
        if diff_act < diff:
            ans = i
            diff = diff_act
    # само число мы получили, теперь нужен индекс
    # print(ans)
    return a.index(ans)


i = 0
j = 0
curr_ub_index = 0
result = []
result_index = []
index_list_un_sorted = []
for i in range(n):
    dist_n = n_list_sorted[i]
    for j in range(m):
        dist_m = m_list_sorted[j]
        curr_ub_index = get_closest_value(dist_n[1],
                                              [x[1] for x in m_list_sorted])
# Вот тут нужно оптимизировать, чтобы обрабатывать не весь список каждый раз,
# А избегать уже пройденные значения, списки ведь уже отсортированы

    # result.append(sortFunc(m_list_sorted[curr_ub_index]))
    # result_index.append(curr_ub_index + 1)
    # result.append(m_length.index(sortFunc(m_list_sorted[curr_ub_index],
    #                                      1)) + 1)
    # Формируем список из индексов для селения и ближайшего убежища в
    # ОТСОРТИРОВАННОМ списке
    index_list_un_sorted.append((dist_n[0], curr_ub_index + 1))
    # Теперь по индексам в отсортированном списке восстанавливаем значения для
    # исходного списка селений
# print(*result)
# print(*result_index)
# print(*index_list_un_sorted)
index_list_sorted = sorted(index_list_un_sorted, key=lambda x: x[0])
# print(*index_list_sorted)
# Работает верно, теперь осталось вывести индексы убежищ согласно введенному
# списку, а не отсортированному

# print(sortFunc(m_list_sorted[8], 0))
res_array = []
for i in range(n):
    res_array.append(sortFunc(m_list_sorted[sortFunc(index_list_sorted[i],
                                                     1) - 1], 0))
print(*res_array)
