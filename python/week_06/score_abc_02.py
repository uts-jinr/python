# Сохраните в массивах список всех участников и выведите его, отсортировав по
# фамилии в лексикографическом порядке. При выводе указываете фамилию, имя
# участника и его балл.
# 30.06.2019 Сдано с 1 раза !
# Убрал структуру Scope и весь соответствующий код


data = []
mysum = 0
inFile = open('input.txt', 'r', encoding='utf8')
for line in inFile:
    myline = line.strip('\n')
    # print(myline)
    # data = myline.split(sep=' ')
    last_n, name, scl, point = myline.split(sep=' ')
    # score.scl = int(scl)
    # score.point = int(point)
    data.append((last_n, name, int(scl), int(point)))
    # print(myline)
inFile.close()
data.sort()
for names in data:
    print(names[0], names[1], names[3])
