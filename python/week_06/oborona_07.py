# В первой строке вводится число n - количество селений (1 <= n <= 100000).
# Вторая строка содержит n различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го селения.
# В третьей строке входных данных задается число m - количество бомбоубежищ
# (1 <= m <= 100000).
# Четвертая строка содержит m различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го бомбоубежища. Все  расстояния
# положительны и не превышают 10⁹.
# Селение и убежище могут располагаться в одной точке.
#
# Чтобы спасение в случае ядерной тревоги проходило как можно эффективнее,
# необходимо для каждого селения определить ближайшее к нему бомбоубежище.
#
# 28.06.2019 Работает но не проходит по времени. Time limit exceeded...
# Удаляем комменты, анализируем код
# Дожал, задача принята.


from math import fabs

# Вводим число селений
n = int(input())
# Формируем список из значений расстояния от НАЧАЛА дороги до селения
# len(n_length) == n
n_length = list(map(int, input().split()))
# Вводим количество убежищ
m = int(input())
# Формируем список значений расстояния от НАЧАЛА дороги до убежища
# len(m_length) == m
m_length = list(map(int, input().split()))

n_list = []
i = 0
for i in range(n):
    n_list.append((n_length[i], i + 1))
n_list.sort()
# print(n_list)
# Создаем список из кортежей [номер убежища, расстояние для убежища]
m_list = []
i = 0
for i in range(m):
    m_list.append((m_length[i], i + 1))

m_list.sort()
# print(m_list)
curr_ub_index = 0
result = []
start = 0
for i in range(n):
    diff = 1000000000
    for j in range(start, m):
        diff_curr = int(fabs(m_list[j][0] - n_list[i][0]))
        # print("j at cycle beginning == ", j)
        if diff_curr < diff:
            diff = diff_curr
            curr_ub_index = j
        if diff_curr == 0:
            curr_ub_index = j
            # print("break with diff_curr == 0, j ==", j)
            start = j - 1
            break
        if diff_curr > diff:
            curr_ub_index = j - 1
            # print("Distance of next is greater than previous, so break, "
            # "j ==", j)
            start = j - 1
            break
    result.append((n_list[i][1], m_list[curr_ub_index][1]))

# print(*result)
result.sort()
# print(*result)
for i in result:
    print(i[1], end=' ')
