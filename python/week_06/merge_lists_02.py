# Даны два целочисленных списка A и B, упорядоченных по неубыванию.
# Объедините их в один упорядоченный список С
# (то есть он должен содержать len(A)+len(B) элементов).
# Решение оформите в виде функции merge(A, B), возвращающей новый список.
# Алгоритм должен иметь сложность O(len(A)+len(B)).
# Модифицировать исходные списки запрещается.
# Использовать функцию sorted и метод sort запрещается.
#
# Программа должна вывести последовательность неубывающих чисел, полученных
# объединением двух данных списков.
#
# Используем упорядоченность по неубыванию !


def merge(copy_a, copy_b):
    c = []
    i_a = 0
    i_b = 0
    a_l = len(copy_a)
    b_l = len(copy_b)

    while (a_l - 1) >= i_a and (b_l - 1) >= i_b:
        if copy_a[i_a] <= copy_b[i_b]:
            c.append(copy_a[i_a])
            i_a += 1
        else:
            c.append(copy_b[i_b])
            i_b += 1

    if a_l > i_a:
        for i in range(i_a, a_l):
            c.append(a_c[i_a])
    elif b_l > i_b:
        for i in range(i_b, b_l):
            c.append(b_c[i_b])

    return c


a = list(map(int, input().split()))
b = list(map(int, input().split()))

a_c = a.copy()
b_c = b.copy()
C = []

# передаем в ф-цию сначала меньший список потом больший
# if len(a_c) <= len(b_c):
#    C = merge(a_c, b_c)
# else:
#    C = merge(b_c, a_c)

# print(C) == [1, 2, 4, 4, 5, 5, 7]
C = merge(a_c, b_c)
print(*C)
