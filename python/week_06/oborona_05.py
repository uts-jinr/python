# В первой строке вводится число n - количество селений (1 <= n <= 100000).
# Вторая строка содержит n различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го селения.
# В третьей строке входных данных задается число m - количество бомбоубежищ
# (1 <= m <= 100000).
# Четвертая строка содержит m различных целых чисел, i-е из этих чисел задает
# расстояние от начала дороги до i-го бомбоубежища. Все  расстояния
# положительны и не превышают 10⁹.
# Селение и убежище могут располагаться в одной точке.
#
# Чтобы спасение в случае ядерной тревоги проходило как можно эффективнее,
# необходимо для каждого селения определить ближайшее к нему бомбоубежище.
#
# 28.06.2019 Работает но не проходит по времени. Time limit exceeded...


from math import fabs


# принимает кортеж, индекс элемента в кортеже, возвращает элемент кортежа по
# индексу
# def sortFunc(obj, index):
#    return obj[index]


# Вводим число селений
n = int(input())
# Формируем список из значений расстояния от НАЧАЛА дороги до селения
# len(n_length) == n
n_length = list(map(int, input().split()))
# Вводим количество убежищ
m = int(input())
# Формируем список значений расстояния от НАЧАЛА дороги до убежища
# len(m_length) == m
m_length = list(map(int, input().split()))

# СОздать список кортежей, Где кортеж - это пара [индекс селения, расстояние]
# Отсортировать список по расстоянию...
# Создайте список кортежей из пар (позиция селения,
# его номер в исходном списке) - т.е. номер - это индекс,
# а позициия - расстояние от начала дороги

# print(*n_length)

n_list = []
# i = 0
for i in range(n):
    #              расстояние,  индекс
    n_list.append((n_length[i], i + 1))
    #
    # Сортируем список из кортежей по 1-му эл-ту кортежа, т.е. по расстоянию
    # Сохраняем в отдельный список
    # CCуки, почему не сказали про ф-цию lambda ???
    # n_list_sorted = sorted(n_list, key=lambda x: x[1])
    n_list_sorted = sorted(n_list)
# печатаем после цикла создания массива
# print(*n_list_sorted)
# print(sorted(n_list, key=lambda x: x[1]))
#
# Создаем список из кортежей [номер убежища, расстояние для убежища]
m_list = []
i = 0
for i in range(m):
    m_list.append((m_length[i], i + 1))

    # Сортируем список по 1 эл-ту кортежа
    # m_list_sorted = sorted(m_list, key=lambda x: x[1])
    m_list_sorted = sorted(m_list)
# print(*m_list_sorted)
# Теперь проходим по обоим сортированным спискам и формируем ответ
# на задачу, т.е. итоговый список.
#
# Однако поиск минимального расстояния можно сократить, т.к. теперь селения и
# убежища выстроены по возрастания
# Получаем уже решенную ранее задачу:
# В заданной последовательности чисел найти число, ближайшее к заданному


i = 0
j = 0
curr_ub_index = 0
result = []
result_index = []
index_list_un_sorted = []
# print(m_list_sorted[3][0])
# print(m_list_sorted[3][1])
diff = 1000000000
for i in range(n):
    dist_n = n_list_sorted[i][0]
    for j in range(m):
        dist_m = m_list_sorted[j][0]
        diff_curr = int(fabs(dist_m - dist_n))
        if diff_curr < diff:
            diff = diff_curr
            curr_ub_index = j
        if diff_curr == 0:
            curr_ub_index = j
            diff = 1000000000
            # Селение и убежище находятся в 1 точке
            # ближе быть не может, переходим к следующему селению
            break
        if diff_curr > diff:
            curr_ub_index = j - 1
            diff = 1000000000
            break
        # curr_ub_index = get_closest_value(dist_n[1],
        #                                      [x[1] for x in m_list_sorted])
    # Вот тут нужно оптимизировать, чтобы обрабатывать не весь список каждый
    # раз, А избегать уже пройденные значения, списки ведь уже отсортированы

    # result.append(sortFunc(m_list_sorted[curr_ub_index]))
    # result_index.append(curr_ub_index + 1)
    # result.append(m_length.index(sortFunc(m_list_sorted[curr_ub_index],
    #                                      1)) + 1)
    # Формируем список из кортежей -
    # [расстояние для селения][индекс ближайшего убежища]
    # Причем индекс убежища в ОТСОРТИРОВАННОМ СПИСКЕ убежищ !!!
    # ...
    # А теперь пробуем сразу записывать не индекс убежища, а его значение
    # index_list_un_sorted.append((dist_n, curr_ub_index + 1))
    result.append((n_list_sorted[i][1], m_list_sorted[curr_ub_index][1]))
    # Теперь по индексам в отсортированном списке восстанавливаем значения для
    # исходного списка селений
# print(*result)
# print(*result_index)
# print(*index_list_un_sorted)
# index_list_sorted = sorted(index_list_un_sorted, key=lambda x: x[0])
# print(*index_list_sorted)
# Работает верно, теперь осталось вывести индексы убежищ согласно введенному
# списку, а не отсортированному

# print(sortFunc(m_list_sorted[8], 0))

# print(*result)
result.sort()
# print(*result)
for i in result:
    print(i[1], end=' ')
