# Во входном файле (вы можете читать данные из sys.stdin,
# подключив библиотеку sys) записан текст.
# Словом считается последовательность непробельных символов идущих подряд,
# слова разделены одним или большим числом пробелов или символами конца
# строки. Определите, сколько различных слов содержится в этом тексте.
# Тут читаем входные данные из файла чеерез read(), т.е. все строки сразу

inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
# убрали split(sep=' ')
whole_text = whole_text.split()
myset = set(whole_text)
mylist = list(myset)
print(len(mylist))



