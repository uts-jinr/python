# Первая строка входных данных содержит число n — наибольшее число, которое
# мог загадать Август. Далее идут строки, содержащие вопросы Беатрисы. Каждая
# строка представляет собой набор чисел, разделенных пробелами. После каждой
# строки с вопросом идет ответ Августа: YES или NO. Наконец, последняя строка
# входных данных содержит одно слово HELP.


n = int(input())
set_full = set(range(n + 1))

line = input()
while line != "HELP":
    nums_ask = set((map(int, line.split())))
    answer = input()
    if answer == "YES":
        set_full &= nums_ask
        # print(*list(set_full))
    elif answer == "NO":
        # Множество, элементы которого входят в A, но не входят в B
        set_full -= nums_ask
        # print(*list(set_full))
    elif answer == "HELP":
        break
    line = input()
print(*sorted(list(set_full)))
