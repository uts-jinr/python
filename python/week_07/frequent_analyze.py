# Дан текст. Выведите все слова, встречающиеся в тексте, по одному на каждую
# строку. Слова должны быть отсортированы по убыванию их количества появления
# в тексте, а при одинаковой частоте появления — в лексикографическом порядке.


inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
whole_text = whole_text.split()
# sep=' '
words_count = len(whole_text)
words_dic = {}

for i in range(words_count):
    if whole_text[i] not in words_dic:
        words_dic[whole_text[i]] = 1
        # words_order.append(0)
    else:
        words_dic[whole_text[i]] += 1
# print(words_dic)
# Словарь {слово:число} получили, теперь его преобразуем в список кортежей
words_list = []
for words, number in words_dic.items():
    # print(words, number)
    words_list.append((number, words))

words_list.sort(reverse=True)
# words_list.sort(key=lambda tup: tup[1])
print(words_list)
for word in words_list:
    print(word[1])
