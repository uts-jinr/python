# Программа получает на вход количество пар синонимов N. Далее следует N
# строк, каждая строка содержит ровно два слова-синонима.
# После этого следует одно слово.
#
# Программа должна вывести синоним к данному слову.
#


N = int(input())
dic_1 = {}
dic_2 = {}
for i in range(N):
    line = input()
    word_1 = line[:line.find(' ')].strip()
    word_2 = line[line.find(' ') + 1:].strip()
    dic_1[word_1] = []
    dic_2[word_2] = []
    dic_1[word_1].append(word_2)
    dic_2[word_2].append(word_1)
word = input()
if word in dic_1:
    print(*dic_1[word])
elif word in dic_2:
    print(*dic_2[word])
else:
    print("FUCK !")
# print(word_1)
# print(word_2)
# print(dic_1)
# print(dic_2)
