# Дан текст. Выведите слово, которое в этом тексте встречается чаще всего.
# Если таких слов несколько, выведите то, которое меньше в лексикографическом
# порядке.


inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
whole_text = whole_text.split()
# sep=' '
words_count = len(whole_text)
words_dic = {}

for i in range(words_count):
    if whole_text[i] not in words_dic:
        words_dic[whole_text[i]] = 1
        # words_order.append(0)
    else:
        words_dic[whole_text[i]] += 1
# print(words_dic)
# Словарь {слово:число} получили, теперь его преобразуем в список кортежей
words_list = []
for words, number in words_dic.items():
    # print(words, number)
    words_list.append((number, words))
words_list.sort
# Список кортежей получили, отсортировали
print(words_list)
words_list.sort(key=lambda tup: tup[1])
print(words_list)
max_el = max(words_list)

max_list = []
# Получили максимальное значение, вынимаем его из словаря, добавляем вынутое
# в новый словарь и проверяем, осталось ли в исходном словаре такое же max
print(max_el)
# max_list.append(max_el)
# Работает правильно с данными "banana, orange"
for num in words_dic:
    if max_el[1] in num:
        max_list.append(max_el)
        words_list.remove(max_el)
        max_el = max(words_list)
        print(max_el)

print(words_dic)
print(*max_list)
print(*words_list)
max_list.sort(key=lambda tup: tup[1])
print(max_list[0][1])
