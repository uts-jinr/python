# Дан текст. Выведите слово, которое в этом тексте встречается чаще всего.
# Если таких слов несколько, выведите то, которое меньше в
# лексикографическом порядке.


inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
whole_text = whole_text.split(sep=' ')
words_count = len(whole_text)
words_dic = {}
# Словарь получим, преобразуем его в кортеж через items
# Кортеж можно отсортировать :)
for i in range(words_count):
    if whole_text[i] not in words_dic:
        words_dic[whole_text[i]] = 0
        # words_order.append(0)
    else:
        words_dic[whole_text[i]] += 1


