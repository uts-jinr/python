# Каждый из N школьников некоторой школы знает Mᵢ языков. Определите, какие
# языки знают все школьники и языки, которые знает хотя бы один из школьников.
#
# Первая строка входных данных содержит количество школьников N. Далее идет N
# чисел Mᵢ, после каждого из чисел идет Mᵢ строк, содержащих названия языков,
# которые знает i-й школьник. Длина названий языков не превышает 1000
# символов, количество различных языков не более 1000. 1≤N≤1000, 1≤Mᵢ≤500.
#
#
# Формат вывода
#
# В первой строке выведите количество языков, которые знают все школьники.
# Начиная со второй строки - список таких языков. Затем - количество языков,
# которые знает хотя бы один школьник, на следующих строках - список таких
# языков.


# Создаем N разных множеств и смотрим, как они между собой пересекаются.
#
# Получаем количество учеников, это счетчик множеств


N = int(input())
lang_all = set()
lang_one = set()
# Создаем в цикле множества
i = 0
# Создаем список множеств
global_set = []
while i < N:
    # Если введено число X, то идем на ф-цию создания множества размером X \
    # элементов
    Ni = int(input())
    myList = []
    for j in range(1, Ni + 1):
        myList.append(input())
    # Теперь добавляем полученный список в Общий список.
    # Создаем из списка множестов, добавляем его в список
    global_set.append(myList)
    lang_one = set(myList) ^ lang_one
    lang_all = lang_all | set(myList)
    # if i > 1 :
    # lang_one = set(global_set[i]) & set(myList)
    # print(lang_all)
    # print(lang_one)

    i += 1
# Считываем число языков и их название
# Да, так работает !!!!
# print(global_set[2])

lang_one = set(global_set[0])
for j in range(1, N):
    lang_one = lang_one & set(global_set[j])
#
print(len(lang_one))
list_one = list(lang_one)
for i in range(len(lang_one)):
    print(list_one[i])
print(len(lang_all))
list_all = list(lang_all)
for i in range(len(lang_all)):
    print(list_all[i])

# set_1 = {"russian", "spanish"}
# set_2 = {"russian", "english"}
# set_3 = set_1 ^ set_2
# set_4 = set_1 & set_2
# print(set_4)
# print(set_3)
# print((set_1 | set_2) - set_3)
