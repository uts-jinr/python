# Дан список стран и городов каждой страны. Затем даны названия городов.
# Для каждого города укажите, в какой стране он находится.
# Формат ввода
#
# Программа получает на вход количество стран N. Далее идет N строк, каждая
# строка начинается с названия страны, затем идут названия городов этой
# страны. Название каждого город состоит из одного слова. В следующей строке
# записано число M, далее идут M запросов — названия каких-то M городов,
# перечисленных выше.
#
# Формат вывода
#
# Для каждого из запроса выведите название страны, в котором находится
# данный город.
#
# В предыдущих версиях ключом была страна, теперь ключом будет город,
# а значением - страна.

country_num = int(input())
city_country = {}
for i in range(country_num):
    # Добавляем эл-ты в словарь страна:города
    # Сначала считываем строку в список
    line = input()
    country = line[:line.find(' ')].strip()
    city_values = line[line.find(' ') + 1:].split()
    for gorod in city_values:
        if gorod not in city_country:
            city_country[gorod] = []
        city_country[gorod].append(country)
    # country_city[country_key].append(country_values)
    # print()
    # print(country_key)
    # print(country_values)

# Получили данные, сформировали словари. Вывели словарь, идем дальше
# Принимаем ввод M
city_num = int(input())
city_list = []
for i in range(city_num):
    line = input().strip()
    city_list.append(line)
# print(city_country)
# print(city_list)
# Получили список из городов
#
for i in city_list:
    print(*city_country[i])
