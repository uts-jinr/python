# Дан список чисел, который может содержать до 100000 чисел. Определите,
# сколько в нем встречается различных чисел.


mylist = map(int, input().split())
myset = set(mylist)
# print(myset)
n_list = list(myset)
# print(n_list)
print(len(n_list))
