# Дан список стран и городов каждой страны. Затем даны названия городов.
# Для каждого города укажите, в какой стране он находится.
# Формат ввода
#
# Программа получает на вход количество стран N. Далее идет N строк, каждая
# строка начинается с названия страны, затем идут названия городов этой
# страны. Название каждого город состоит из одного слова. В следующей строке
# записано число M, далее идут M запросов — названия каких-то M городов,
# перечисленных выше.
#
# Формат вывода
#
# Для каждого из запроса выведите название страны, в котором находится
# данный город.

country_num = int(input())
country_city = {}
for i in range(country_num):
    # Добавляем эл-ты в словарь страна:города
    # Сначала считываем строку в список
    line = input()
    country_key = line[:line.find(' ')].strip()
    country_values = line[line.find(' ') + 1:].split()
    country_city[country_key] = []
    for cities in country_values:
        country_city[country_key].append(cities)
    # country_city[country_key].append(country_values)
    # print()
    # print(country_key)
    # print(country_values)
# print(country_city)
# Получили данные, сформировали словари. Вывели словарь, идем дальше
# Принимаем ввод M
city_num = int(input())
city_list = []
for i in range(city_num):
    line = input().strip()
    city_list.append(line)
# print(city_list)
# Получили список из городов




