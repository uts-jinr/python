# Даны два списка чисел, которые могут содержать до 100000 чисел каждый.
# Посчитайте, сколько чисел содержится одновременно как в первом списке,
# так и во втором.
#
# Эту задачу на Питоне можно решить в одну строчку.


mylist_1 = map(int, input().split())
mylist_2 = map(int, input().split())
set_1 = set(mylist_1)
set_2 = set(mylist_2)
list_1 = list(set_1)
list_2 = list(set_2)
print(list_1)
print(list_2)

set_1_1 = set(list_1)
set_2_1 = set(list_2)
cross_list = set_1_1 - set_2_1
# Эл-ты входят в set_1, но не входят в set_2
print(cross_list)
# Объединение множеств
cross_list = set_1_1 | set_2_1
print(cross_list)
# сложная выборка множеств
cross_list = set_1_1 ^ set_2_1
print(cross_list)
# Пересечение множеств - вот оно !
cross_list = set_1_1 & set_2_1
print(cross_list)
