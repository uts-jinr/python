# Во входном файле (вы можете читать данные из файла input.txt) записан текст.
# Словом считается последовательность непробельных подряд идущих символов.
# Слова разделены одним или большим числом пробелов или символами конца
# строки. Для каждого слова из этого текста подсчитайте, сколько раз оно
# встречалось в этом тексте ранее.
inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
whole_text = whole_text.split()
# print(whole_text)
words_count = len(whole_text)
# print("words count is", words_count)
words_dic = {}
words_order = []

for i in range(words_count):
    words_order.append(0)
# Однако так короче
# words_order = [0] * words_count


for i in range(words_count):
    if whole_text[i] not in words_dic:
        words_dic[whole_text[i]] = 0
        # words_order.append(0)
    else:
        words_dic[whole_text[i]] += 1
        words_order[i] += words_dic[whole_text[i]]
#
# for i in words_dic:
# print(words_dic[i])
print(*words_order)
