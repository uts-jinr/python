# Во входном файле (вы можете читать данные из файла input.txt) записан текст.
# Словом считается последовательность непробельных подряд идущих символов.
# Слова разделены одним или большим числом пробелов или символами конца
# строки. Для каждого слова из этого текста подсчитайте, сколько раз оно
# встречалось в этом тексте ранее.
inFile = open('input.txt', 'r', encoding='utf8')
whole_text = inFile.read()
whole_text = whole_text.strip()
whole_text = whole_text.split(sep=' ')
print(whole_text)
words_count = len(whole_text)
print("words count is", words_count)
words_dic = {}
words_order = []

for i in range(words_count):
    if whole_text[i] not in words_dic:
        words_dic[whole_text[i]] = 0
    else:
        words_dic[whole_text[i]] = words_dic[whole_text[i]] + 1

for line in whole_text:
    words_order.append(0)
    if line not in words_dic:
        words_dic[line] = 0
        # words_dic[line].append(0)
    else:
        value = words_dic[line] + 1
        words_dic[line] = value
#
for i in words_dic:
    print(words_dic[i])
