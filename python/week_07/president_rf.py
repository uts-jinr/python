# В выборах Президента Российской Федерации побеждает кандидат, набравший
# свыше половины числа голосов избирателей. Если такого кандидата нет, то во
# второй тур выборов выходят два кандидата, набравших наибольшее число голосов
#
# Каждая строка входного файла содержит имя кандидата, за которого отдал голос
# один избиратель. Известно, что общее число кандидатов не превосходит 20,
# но в отличии от предыдущих задач список кандидатов явно не задан. Читайте
# данные из файла input.txt с указанием кодировки utf8.

inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
pretend_vote = {}
pretend_list = []
vote_count = 0
for line in inFile:
    line = line.strip()
    vote_count += 1
    if line not in pretend_vote:
        pretend_vote[line] = 1
    else:
        pretend_vote[line] += 1
for name, votes in pretend_vote.items():
    pretend_list.append((votes, name))

half_percent = vote_count / 2
name_list = []
pretend_list.sort(reverse=True)
for man in pretend_list:
    if man[0] > half_percent:
        name = man[1]
        name_list.append(name)
        break
    # else:
    # name = "@@frik@@"

if not name_list:
    print(pretend_list[0][1], file=outFile)
    print(pretend_list[1][1], file=outFile)
else:
    print(name, file=outFile)

# print(name)


# print(pretend_vote)
# print(*pretend_list)
# print("Votes number is ", vote_count)
# print("50%  of votes is", vote_count/2)
inFile.close()
outFile.close()
