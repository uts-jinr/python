# По данному натуральному n вычислите сумму 1²+2²+3²+...+n².

n = int(input())
i = 1
sum = 0
while i <= n:
    sum += i * i
    i += 1
print(sum)
