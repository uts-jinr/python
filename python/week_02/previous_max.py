# Последовательность состоит из натуральных чисел и завершается числом 0.
# Определите значение второго по величине элемента в этой последовательности,
# то есть элемента, который будет наибольшим, если из последовательности
# удалить одно вхождение наибольшего элемента.

val_prev = int(input())
val_curr = int(input())

if val_prev > val_curr:
    (val_prev, val_curr) = (val_curr, val_prev)

val = int(input())
while val != 0:
    if val_prev < val < val_curr:
        val_prev = val
    elif val >= val_curr:
        val_prev = val_curr
        val_curr = val
    val = int(input())
#    if val == 0:
#        break


print(val_prev)
