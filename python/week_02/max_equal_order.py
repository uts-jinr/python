# Определите, какое наибольшее число подряд идущих элементов этой
# последовательности равны друг другу.

el = int(input())
n = 1
m = 0
equal_value = 0
max_number = 1
while el != 0:
    if el == equal_value:
        n += 1
        if n > max_number:
            max_number = n
    else:
        equal_value = el
#        if n > max_number:
#            max_number = n
        n = 1
    el = int(input())
print(max_number)
