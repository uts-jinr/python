# Определите сумму всех элементов последовательности, завершающейся
# числом 0.

val = int(input())
summ_seq = 0

while val != 0:
    summ_seq += val
    val = int(input())
print(summ_seq)
