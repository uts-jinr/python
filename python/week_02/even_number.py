# Определите количество четных элементов в последовательности,
# завершающейся числом 0.
val = int(input())
even_number = 0
while val != 0:
    if val % 2 == 0:
        even_number += 1
    val = int(input())
print(even_number)
