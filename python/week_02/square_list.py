N = int(input())
k = 1
line = ""
while (k * k) <= N:
    sq = k * k
    line = line + str(sq) + " "
    k = k + 1
print(line)
