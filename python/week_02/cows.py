N = int(input())
# Далее вычисляем последнюю цифру

ld = N % 10  # last digit

if (10 <= N <= 20):
    lc = ""
elif ((ld == 0) or (5 <= ld <= 9)):
    lc = ""
elif (ld == 1):
    lc = "a"
elif (2 <= ld <= 4):
    lc = "y"
print(N, " korov", lc, sep='')
