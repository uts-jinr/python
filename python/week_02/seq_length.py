val = int(input())
if val != 0:
    seq_length = 1
else:
    seq_length = 0
while val != 0:
    val = int(input())
    if val == 0:
        break
    seq_length += 1

print(seq_length)
