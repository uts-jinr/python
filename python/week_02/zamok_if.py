a, b, c = int(input()), int(input()), int(input())
d, e = int(input()), int(input())

if (((a <= d) and (b <= e)) or
        ((b <= d) and (c <= e)) or
        ((c <= d) and (a <= e))):
    print("YES")
elif (((a <= e) and (b <= d)) or
      ((b <= e) and (c <= d)) or
      ((c <= e) and (a <= d))):
    print("YES")
else:
    print("NO")
