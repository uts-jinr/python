a1, b1, c1 = int(input()), int(input()), int(input())
a2, b2, c2 = int(input()), int(input()), int(input())

# if (((a1 == a2) and (b1 == b2) and (c1 == c2)) or
#        ((a1 == b2) and (b1 == a2) and (c1 == c2)) or
#        ((a1 == b2) and (b1 == c2) and (c1 == b2)) or
#        ((a1 == c2) and (b1 == b2) and (c1 == c2)) or
#        ((a1 == c2) and (b1 == c2) and (c1 == b2))):
# print("Boxes are equal")

# elif ((a1 < a2) or (a1 < b2) or (a1 < c2) or
#      (b1 < a2) or (b1 < b2) or (b1 < c2) or
#      (c1 < a2) or (c1 < b2) or (c1 < c2)):
# print("The first box is smaller than the second one")
# else:
#    print("The first box is larger than the second one")

# !!!!!!!!!!!!!!!!!!!!!!!!!!! REMEMBER order_3.py !!!!!!!!!!!!!!!!!!!
if (b1 < a1):
    (a1, b1) = (b1, a1)
#  so  a <= b
if (c1 < b1):
    (b1, c1) = (c1, b1)
# so b <= c
if (c1 < a1):
    (c1, a1) = (a1, c1)
if (b1 < a1):
    (a1, b1) = (b1, a1)
# now a1 ≤ b1 ≤ c1
# ###########################3
if (b2 < a2):
    (a2, b2) = (b2, a2)
#  so  a <= b
if (c2 < b2):
    (b2, c2) = (c2, b2)
# so b <= c
if (c2 < a2):
    (c2, a2) = (a2, c2)
if (b2 < a2):
    (a2, b2) = (b2, a2)
# now  a2 ≤ b2 ≤ c2
# Go ahead

# print(a1, "   ", a2)
# print(b1, "   ", b2)
# print(c1, "   ", c2)
if a1 == a2 and b1 == b2 and c1 == c2:
    print("Boxes are equal")
elif (((c1 < c2) and ((b1 <= b2) and (a1 <= a2))) or
      ((c1 == c2) and ((b1 < b2) and (a1 <= a2))) or
      ((c1 == c2) and ((b1 == b2) and (a1 < a2)))):
    print("The first box is smaller than the second one")
elif (((c1 > c2) and ((b1 >= b2) and (a1 >= a2))) or
      ((c1 == c2) and ((b1 > b2) and (a1 >= a2))) or
      ((c1 == c2) and ((b1 == b2) and (a1 > a2)))):
    print("The first box is larger than the second one")
else:
    print("Boxes are incomparable")
