# Последовательность состоит из целых чисел и завершается числом 0.
# Определите значение наибольшего элемента последовательности.

val = int(input())
max = val
while (val != 0):
    val = int(input())
    if val == 0:
        break
    if val > max:
        max = val
print(max)
