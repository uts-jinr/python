# Количество элементов, равных максимуму

el = int(input())
n = 0
max_value = 0
while el != 0:
    if el == max_value:
        n += 1
    if el > max_value:
        max_value = el
        n = 1
    el = int(input())
print(n)
