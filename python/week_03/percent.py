# Процентная ставка по вкладу составляет P процентов годовых, которые
# прибавляются к сумме вклада. Вклад составляет X рублей Y копеек.
# пределите размер вклада через год. При решении этой задачи нельзя
# пользоваться условными инструкциями и циклами.
# from math import floor, ceil

p = float(input())
rub = float(input())
cop = float(input())

vklad = rub * 100 + cop
vklad /= 100
#  print(vklad)
x = vklad + (vklad / 100) * p
drob_part = int((x * 100) % 100)
print(int(x), drob_part)
