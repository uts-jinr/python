# Дана строка. Найдите в этой строке второе вхождение буквы f и выведите
# индекс этого вхождения. Если буква f в данной строке встречается только один
# раз, выведите число -1, а если не встречается ни разу, выведите число -2.
# При решении этой задачи нельзя использовать метод count.
ek_str = input()
substr = "f"
length = len(ek_str)
pos = ek_str.find(substr)
if pos < 0:
    result = -2
else:
    e_substr = ek_str[(pos + 1):]
#    print(e_substr)
    pos_2 = e_substr.find(substr)
    if pos_2 >= 0:
        result = length - len(e_substr) + pos_2
    else:
        result = -1
print(result)
