n = float(input())

i = 1
sum_range = 0
while i <= n:
    sum_range += (1 / i ** 2)
    i += 1

print(sum_range)
