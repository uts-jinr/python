# Дана строка. Если в этой строке буква f встречается только один раз,
# выведите её индекс. Если она встречается два и более раз, выведите индекс
# её первого и последнего появления. Если буква f в данной строке не
# встречается, ничего не выводите. При решении этой задачи нельзя
# использовать метод count и циклы.
# Последнее вхождение - это первое вхождение с конца строки !
ek_str = input()
substr = "f"
pos = ek_str.find(substr)
pos_2 = ""
if pos >= 0:
    #    print(pos)
    # делаем реверс строки
    reverse = ek_str[::-1]
    pos_2 = reverse.find(substr)
    #    print("length = ", len(ek_str))
    #    print("pos_2 =", pos_2)
    last_pos = len(ek_str) - pos_2 - 1

    if last_pos == pos:
        pos_2 = ""
    else:
        pos_2 = last_pos
    print(pos, pos_2)
