# Даны вещественные числа a, b, c, d, e, f.
# Известно, что система линейных уравнений:
#
# ax + by = e
#
# cx + dy = f
#
# имеет ровно одно решение.
# Выведите два числа x и y, являющиеся решением этой системы.
a = float(input())
b = float(input())
c = float(input())
d = float(input())
e = float(input())
f = float(input())

if a == 0 and b != 0 and c != 0:
    y = e / b
    x = (f - d * y) / c
elif b == 0 and a != 0 and d != 0:
    x = e / a
    y = (f - c * x) / d
elif c == 0 and a != 0 and d != 0:
    y = f / d
    x = (e - b * y) / a
elif d == 0 and b != 0 and c != 0:
    x = f / c
    y = (e - a * x) / b
else:
    y = (f * a - c * e) / (d * a - c * b)
    x = (e - b * y) / a
print(x, y)
