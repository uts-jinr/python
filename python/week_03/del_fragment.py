# Дана строка, в которой буква h встречается минимум два раза.Удалите из этой
# строки первое и последнее вхождение буквы
# h,а также все символы, находящиеся между ними.
ek_str = input()
substr = "h"
length = len(ek_str)
pos = ek_str.find(substr)
str_begin = ek_str[:pos]
# print(str_begin)
# pos_2 = ""
# делаем реверс строки
reverse = ek_str[::-1]
# Позиция с конца, последняя
pos_2 = reverse.find(substr)
if pos_2 == 0:
    str_end = ""
else:
    #    print("reverse pos_2 =", pos_2)
    str_end = ek_str[((pos_2) * (-1)):]
#    print(str_end)
print(str_begin, str_end, sep='')
