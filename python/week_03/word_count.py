# Дана строка, состоящая из слов, разделенных пробелами.
# Определите, сколько в ней слов.
ek_str = input()
substr = " "
length = len(ek_str)
word_count = ek_str.count(substr)
space_pos = ek_str.rfind(substr)
# print("Most right space in position", space_pos)
# print("word count is", word_count + 1)
# Нужен цикл по удалению пробелов в конце строки
# print("String length", length)
# Удаляем пробелы в начале строки
left_str = ek_str
# print("Str copy is :", left_str)
left_space_pos = left_str.find(substr)
while left_space_pos == 0:
    left_str = left_str[1:]
    left_space_pos = left_str.find(substr)
#    print("one more left space")

# #################################################
# Удаляем пробелы в конце строки
ek_str = left_str
while space_pos + 1 == length:
    ek_str = ek_str[:-1]
    length = len(ek_str)
    space_pos = ek_str.rfind(substr)
# print(ek_str)
word_count = ek_str.count(substr)
print(word_count + 1)
