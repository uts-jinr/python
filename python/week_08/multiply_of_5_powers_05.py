from math import pow
from functools import reduce
import operator

print(
    reduce(
        operator.mul,
        list(
            map(
                lambda x: int(pow(x, 5)),
                list(
                    map(
                        int,
                        input().split()
                    )
                )
            )
        ),
        1
    )
)
