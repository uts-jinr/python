# На вход подаётся последовательность натуральных чисел длины n≤1000.
# Посчитайте произведение пятых степеней чисел в последовательности.
# Добавил библиотечную ф-цию pow. Идем дальше.

from math import pow
from functools import reduce


data = list(map(int, input().split()))
print(*data)
b = 1
for i in range(len(data)):
    b *= pow(data[i], 5)

print(int(b))