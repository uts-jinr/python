# На вход подаётся последовательность натуральных чисел длины n≤1000.
# Посчитайте произведение пятых степеней чисел в последовательности.


def power_5(a):
    b = 1
    for i in range(5):
        b = b*a
    return b


data = list(map(int, input().split()))
print(*data)
print(power_5(data[2]))
b = 1
for i in range(len(data)):
    b *= power_5(data[i])

print(b)