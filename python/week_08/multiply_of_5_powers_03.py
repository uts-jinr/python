# На вход подаётся последовательность натуральных чисел длины n≤1000.
# Посчитайте произведение пятых степеней чисел в последовательности.
# Добавил библиотечную ф-цию pow. Идем дальше.
# ПОчти Заменил цикл for через lambda
# Совсем заменил цикл for через reduce(operator.mul. Идем дальше

from math import pow
from functools import reduce
import operator
# from itertools import accumulate


data = list(map(int, input().split()))

p_list = list(map(lambda x: int(pow(x, 5)), data))
print(*p_list)
print(reduce(operator.mul, p_list, 1))

