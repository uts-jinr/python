# На вход подаётся последовательность натуральных чисел длины n≤1000.
# Посчитайте произведение пятых степеней чисел в последовательности.
# Добавил библиотечную ф-цию pow. Идем дальше.
# ПОчти Заменил цикл for через lambda

from math import pow
from functools import reduce
# from itertools import accumulate


data = list(map(int, input().split()))
# print(*data)
b = 1
for i in range(len(data)):
    b *= pow(data[i], 5)

p_list = list(map(lambda x: int(pow(x, 5)), data))
print(int(b))
print(*p_list)

