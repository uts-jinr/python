# Даны три целых числа. Найдите наибольшее из них (программа должна вывести
# ровно одно целое число).

A = int(input())
B = int(input())
C = int(input())
# if A == B and B == C:
#    print(A)

# A < B < C
# A < C < B
# B < A < C
# B < C < A
# C < A < B
# C < B < A
# Код ниже храним как зеницу ока.
# Заработало раза с 15
if ((A <= B < C) or (B < A <= C) or (A < B <= C)):
    print(C)
if ((A <= C < B) or (C < A <= B)):
    print(B)
if ((B <= C < A) or (C < B < A)):
    print(A)

if (A == B and A == C):
    print(A)
    # if A >= B and A > C:
    #    print(A) or
    # if A >= B and A < C:
    #    print(C)

    # if B > A and B >= C:
    #    print(B)
    # if B >= A and B < C:
    #    print(C)
    # if C >= A and C > B:
    #   print(C)
    # if C >= A and C < B:
    #    print(B)
