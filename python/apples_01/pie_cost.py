A = int(input())  # Цена, рубли
B = int(input())  # цена, копейки
N = int(input())  # сколько надо пирожков, штуки

kop_total = (A * 100) + B  # kopeek per pie
cost_total = kop_total * N  # total price for N pies
rub_final = cost_total // 100
kop_final = cost_total % 100

print(rub_final, kop_final)
