fst_hours = int(input())
fst_min = int(input())
fst_sec = int(input())
scnd_hours = int(input())
scnd_min = int(input())
scnd_sec = int(input())

# Now convert data to seconds
fst_total = (fst_hours * 3600) + (fst_min * 60) + fst_sec
scnd_total = (scnd_hours * 3600) + (scnd_min * 60) + scnd_sec

diff = scnd_total - fst_total
# Вот получили количество секунд, далее как в задаче el_clock_02.py
print(diff)
