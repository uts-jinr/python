# Переставьте соседние элементы списка (A[0] c A[1],A[2] c A[3] и т.д.).
# Если элементов нечетное число, то последний элемент остается на своем месте.


numList = list(map(int, input().split()))
length = len(numList)
swap_list = []
# if length % 2 != 0:
#    length -= 1
for i in range(0, length - 1, 2):
    swap_list.append(numList[i + 1])
    swap_list.append(numList[i])

if length % 2 != 0:
    swap_list.append(numList[length - 1])

print(*swap_list)
