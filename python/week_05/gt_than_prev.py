# Дан список чисел. Выведите все элементы списка, которые больше предыдущего
# элемента.


numList = list(map(int, input().split()))
num = len(numList)
for i in range(1, num):
    if numList[i] > numList[i - 1]:
        print(numList[i], end=" ")
