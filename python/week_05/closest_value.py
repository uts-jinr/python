# Напишите программу, которая находит в массиве элемент, самый близкий
# по величине к данному числу.
#
# В первой строке задается одно натуральное число N, не превосходящее 1000
# – размер массива. Во второй строке содержатся N чисел – элементы массива
# (целые числа, не превосходящие по модулю 1000). В третьей строке вводится
# одно целое число x, не превосходящее по модулю 1000.
#
# Вывести значение элемента массива, ближайшее к x. Если таких чисел
# несколько, выведите любое из них.

length = int(input())
numList = list(map(int, input().split()))
source_value = int(input())

length = len(numList)

if length == 1:
    closest_value = numList[0]
else:
    closest_value = 0
a = 0
b = 0
min_diff = 2000
numList.sort()
# print(numList)
i = length - 1
# Если всего 1 элемент в массиве
if source_value >= numList[i]:
    closest_value = numList[i]
else:
    while i > 0:
        if source_value == numList[i]:
            closest_value = numList[i]
        elif numList[i - 1] <= source_value <= numList[i]:
            a = source_value - numList[i - 1]
            b = numList[i] - source_value
            if (a > b) and (b < min_diff):
                closest_value = numList[i]
                min_diff = a - b
            elif (b >= a) and (a <= min_diff):
                closest_value = numList[i - 1]
                min_diff = b - a
        elif source_value < numList[i - 1] and source_value < numList[i]:
            if (numList[i - 1] - source_value) <= min_diff:
                closest_value = numList[i - 1]
                min_diff = numList[i - 1] - source_value
            elif (numList[i] - source_value) <= min_diff:
                closest_value = numList[i]
                min_diff = numList[i] - source_value

        i -= 1
print(closest_value)
