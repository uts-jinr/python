# join
# split
# map
# Выведите все элементы списка с четными индексами
# (то есть A[0], A[2], A[4], ...). Программа должна быть эффективной и
# не выполнять лишних действий!

numList = list(map(int, input().split()))
num = len(numList)
for i in range(0, int((num - 1) / 2) + 1):
    print(numList[i * 2])
# print(num)
# print(numList)
