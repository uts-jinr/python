from math import fabs
l = int(input())
a = list(map(int, input().split()))
num = int(input())
diff = 2000
for i in a:
    diff_act = int(fabs(i - num))
    if diff_act < diff:
        ans = i
        diff = diff_act
print(ans)