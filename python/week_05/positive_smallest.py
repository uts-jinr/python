# Выведите значение наименьшего из всех положительных элементов в списке.
# Известно, что в списке есть хотя бы один положительный элемент, а значения
# всех элементов списка по модулю не превосходят 1000.


numList = list(map(int, input().split()))
num = len(numList)
min_value = 1000
index = 0
for i in range(0, num):
    if 0 < numList[i] < min_value:
        min_value = numList[i]
        index = i
print(numList[index])
