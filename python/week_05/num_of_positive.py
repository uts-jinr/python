# Найдите количество положительных элементов в данном списке.


numList = list(map(int, input().split()))
num = len(numList)
pos_num = 0
for i in range(0, num):
    if numList[i] > 0:
        pos_num += 1

print(pos_num)
