# Найдите наибольшее значение в списке и индекс последнего элемента, который
# имеет данное значение за один проход по списку, не модифицируя этот список
# и не используя дополнительного списка.
#
# Выведите два значения.


numList = list(map(int, input().split()))
num = len(numList)
max_value = numList[0]
max_index = 0
for i in range(1, num):
    if numList[i] >= max_value:
        max_index = i
        max_value = numList[i]
print(max_value, max_index)
