# В списке все элементы попарно различны. Поменяйте местами минимальный и
# максимальный элемент этого списка.


numList = list(map(int, input().split()))
length = len(numList)
buf = numList.copy()
buf.sort()
# print(*buf)
max_value = buf[length - 1]
buf.reverse()
min_value = buf[length - 1]
ind_max = numList.index(max_value)
# print(ind_max)
ind_min = numList.index(min_value)
# print(ind_min)
buf = numList.copy()
buf[ind_max] = numList[ind_min]
buf[ind_min] = numList[ind_max]
print(*buf)
