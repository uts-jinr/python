# ПРограмма считывает два целых числа A и B и выводит наибольшее значение из
# них. Числа — целые от 1 до 1000.
#
# При решении задачи можно пользоваться только целочисленными арифметическими
# операциями.
# Нельзя пользоваться нелинейными конструкциями: ветвлениями, циклами,
# функциями.
A = int(input())
B = int(input())

# Пусть A > B
# Числа — целые от 1 до 1000, поэтому деления на ноль не возникнет.
# A = AC * B + AD
AC = A // B  # целое число - 1 или больше
AD = A % B  # остаток 0 или целое число, а если A < B, то остаток будет само
# число А

# B = (BC * A) + BD
BC = B // A
BD = B % A

# 1. Выдаем ответ в виде (A * N) + (B * P), Где N и P принимают значения 0 или 1
# в нужной зависимости.
# ОСтаток меньше или равен Делимому, значит если поделим остаток на делимое
# получим либо 0(если меньше делимого ), либо 1 (если равен делимому)
# result_01 = ((AC * B) + AD) * (((AD // A) + 1) % 2) +
#             ((BC * A) + BD) * (((BD // B) + 1) % 2)
value_01_00 = ((AC * B) + AD) * (((AD // A) + 1) % 2)  # A-part
value_01_01 = ((AD // A) + 1) % 2  # Множитель в 3 слагаемом
value_02_00 = ((BC * A) + BD) * (((BD // B) + 1) % 2)  # B-part
value_02_01 = ((BD // B) + 1) % 2
# result = value_01 + value_02 - X  # (Где X - само число, если A == B,
# либо 0, если A != B)
# А в итоге формулу [1.] приводим к виду
# result = (value_01 + value_02) / X Где Х = 1 если числа разные и X = 2 если
# числа одинаковые
# A * AC  # AC = 0 если А < B, AC = 1, if A == B

# print(((AC * B) + AD) * (((AD // A) + 1) % 2) +
#       ((BC * A) + BD) * (((BD // B) + 1) % 2))

result = (value_01_00 + value_02_00) // (value_01_01 + value_02_01)
print(result)
